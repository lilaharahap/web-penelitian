<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianAkhirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_akhirs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pengajuan');
            $table->string('kesesuaian_hasil');
            $table->string('luaran_penelitian');
            $table->string('kesimpulan_penilai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_akhirs');
    }
}
