<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLapAkhirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lap_akhirs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pengajuan');
            $table->string('latarbelakang_penelitian');
            $table->string('tujuan_penelitian');
            $table->string('metode_penelitian');
            $table->string('upload_file_penelitian');
            $table->string('hasil_penelitian');
            $table->string('luaran_penelitian');
            $table->string('daftar_pustaka');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lap_akhirs');
    }
}
