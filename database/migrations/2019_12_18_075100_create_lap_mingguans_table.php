<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLapMingguansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lap_mingguans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pengajuan');
            $table->string('daftar_pekerjaan_mingguini');
            $table->string('hasil_capaian_mingguini');
            $table->string('kesesuaian_hasil');
            $table->string('rekomendasi_timpeneliti');
            $table->string('backlog');
            $table->string('rencana_minggudepan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lap_mingguans');
    }
}
