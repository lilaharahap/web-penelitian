<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pengguna');
            $table->string('Latarbelakang_penelitian');
            $table->string('Pendukung Penelitian');
            $table->string('Tujuan_penelitian');
            $table->string('Judul _proposal');
            $table->date('Tanggal_penerbitan');
            $table->string('Metode_penelitian');
            $table->string('Targetcapaian_luaranpenelitian');
            $table->integer('RAB_sumberpendanaan');
            $table->string('Jadwal_kegiatan');
            $table->string('Hasil_penilaian');
            $table->string('Tim_pelaksanan');
            $table->string('Nama_dosen_1');
            $table->string('Nama_dosen_2');
            $table->string('Nama_dosen_3');
            $table->date('Tanggal_pengusul');
            $table->string('Jabatan_dosen_1');
            $table->string('Jabatan_dosen_2');
            $table->string('Jabatan_dosen_3');
            $table->string('Lembaga');
            $table->string('Alamat');
            $table->integer('Telepon');
            $table->string('Faks');
            $table->string('Email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuans');
    }
}
