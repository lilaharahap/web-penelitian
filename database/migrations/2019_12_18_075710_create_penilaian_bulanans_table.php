<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianBulanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_bulanans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pengajuan');
            $table->string('judul_penelitian');
            $table->string('nama_peneliti');
            $table->string('priode_penilaian');
            $table->string('kemajuan_pekerjaan');
            $table->string('kesesuaian_hasil');
            $table->integer('total_nilai');
            $table->string('rekomendasi_peneliti');
            $table->date('tgl_penilaian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_bulanans');
    }
}
