<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_proposals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pengguna');
            $table->date('tanggal_penilaian');
            $table->string('Revisi');
            $table->date('tanggal_revisi');
            $table->string ('perbaikan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_proposals');
    }
}
