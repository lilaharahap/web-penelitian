<?php

namespace App\Http\Controllers;

use App\lap_mingguan;
use Illuminate\Http\Request;

class LapMingguanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\lap_mingguan  $lap_mingguan
     * @return \Illuminate\Http\Response
     */
    public function show(lap_mingguan $lap_mingguan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\lap_mingguan  $lap_mingguan
     * @return \Illuminate\Http\Response
     */
    public function edit(lap_mingguan $lap_mingguan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lap_mingguan  $lap_mingguan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lap_mingguan $lap_mingguan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\lap_mingguan  $lap_mingguan
     * @return \Illuminate\Http\Response
     */
    public function destroy(lap_mingguan $lap_mingguan)
    {
        //
    }
}
