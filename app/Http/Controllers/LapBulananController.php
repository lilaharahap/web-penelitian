<?php

namespace App\Http\Controllers;

use App\lap_bulanan;
use Illuminate\Http\Request;

class LapBulananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\lap_bulanan  $lap_bulanan
     * @return \Illuminate\Http\Response
     */
    public function show(lap_bulanan $lap_bulanan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\lap_bulanan  $lap_bulanan
     * @return \Illuminate\Http\Response
     */
    public function edit(lap_bulanan $lap_bulanan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lap_bulanan  $lap_bulanan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lap_bulanan $lap_bulanan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\lap_bulanan  $lap_bulanan
     * @return \Illuminate\Http\Response
     */
    public function destroy(lap_bulanan $lap_bulanan)
    {
        //
    }
}
