<?php

namespace App\Http\Controllers;

use App\penilaian_bulanan;
use Illuminate\Http\Request;

class PenilaianBulananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\penilaian_bulanan  $penilaian_bulanan
     * @return \Illuminate\Http\Response
     */
    public function show(penilaian_bulanan $penilaian_bulanan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\penilaian_bulanan  $penilaian_bulanan
     * @return \Illuminate\Http\Response
     */
    public function edit(penilaian_bulanan $penilaian_bulanan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\penilaian_bulanan  $penilaian_bulanan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, penilaian_bulanan $penilaian_bulanan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\penilaian_bulanan  $penilaian_bulanan
     * @return \Illuminate\Http\Response
     */
    public function destroy(penilaian_bulanan $penilaian_bulanan)
    {
        //
    }
}
