<?php

namespace App\Http\Controllers;

use App\persetujuan;
use Illuminate\Http\Request;

class PersetujuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\persetujuan  $persetujuan
     * @return \Illuminate\Http\Response
     */
    public function show(persetujuan $persetujuan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\persetujuan  $persetujuan
     * @return \Illuminate\Http\Response
     */
    public function edit(persetujuan $persetujuan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\persetujuan  $persetujuan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, persetujuan $persetujuan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\persetujuan  $persetujuan
     * @return \Illuminate\Http\Response
     */
    public function destroy(persetujuan $persetujuan)
    {
        //
    }
}
