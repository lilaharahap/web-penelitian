<?php

namespace App\Http\Controllers;

use App\penilaian_akhir;
use Illuminate\Http\Request;

class PenilaianAkhirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\penilaian_akhir  $penilaian_akhir
     * @return \Illuminate\Http\Response
     */
    public function show(penilaian_akhir $penilaian_akhir)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\penilaian_akhir  $penilaian_akhir
     * @return \Illuminate\Http\Response
     */
    public function edit(penilaian_akhir $penilaian_akhir)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\penilaian_akhir  $penilaian_akhir
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, penilaian_akhir $penilaian_akhir)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\penilaian_akhir  $penilaian_akhir
     * @return \Illuminate\Http\Response
     */
    public function destroy(penilaian_akhir $penilaian_akhir)
    {
        //
    }
}
