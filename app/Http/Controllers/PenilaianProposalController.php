<?php

namespace App\Http\Controllers;

use App\penilaian_proposal;
use Illuminate\Http\Request;

class PenilaianProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\penilaian_proposal  $penilaian_proposal
     * @return \Illuminate\Http\Response
     */
    public function show(penilaian_proposal $penilaian_proposal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\penilaian_proposal  $penilaian_proposal
     * @return \Illuminate\Http\Response
     */
    public function edit(penilaian_proposal $penilaian_proposal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\penilaian_proposal  $penilaian_proposal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, penilaian_proposal $penilaian_proposal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\penilaian_proposal  $penilaian_proposal
     * @return \Illuminate\Http\Response
     */
    public function destroy(penilaian_proposal $penilaian_proposal)
    {
        //
    }
}
